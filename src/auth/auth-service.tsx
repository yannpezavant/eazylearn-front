import axios from "axios";
import { User } from "../entities";

export  async function register(user:User) {
    const response = await axios.post<{user:User, token:string}>('/api/user', user);
    return response.data;
}

export  async function login(mail:string, password:string) {
    const response = await axios.post<{token:string}>('/api/login', {mail,password});
    return response.data;
}

export async function fetchUser() {  
    const response = await axios.get<User>('/api/account');
    return response.data;
}





