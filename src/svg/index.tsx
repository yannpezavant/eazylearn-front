export {default as IconAccount} from './IconAccount.svg';
export {default as Loupe} from './Loupe.svg';
export {default as Telephone} from './Telephone.svg';
export {default as Calendrier} from './Calendrier.svg';
export {default as Planificateur} from './Planificateur.svg';
export {default as Event} from './Event.svg';