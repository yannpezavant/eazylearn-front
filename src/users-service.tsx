import axios from "axios";
import { User } from "./entities";




export const userService = {
    async fetchAllUsers(){
        const response = await axios.get<User[]>("/api/user");
        return response.data;
    }
}

export const userByIdService = {
    async getUserById(id: number | string){
        const response = await axios.get<User[]>("/api/user" + id);
        return response.data;
    }
}


