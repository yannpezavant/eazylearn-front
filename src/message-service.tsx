import axios from "axios";
import { Message} from "./entities";

export async function postMessage(message:Message) {
    const response = await axios.post<Message>('/api/message', message);
    return response.data;
}
export async function updateMessage(message:Message){
    const response = await axios.put<Message>('/api/message/'+ message.id, message);
    return response.data;
}
export async function deleteMessage(id:any){
    await axios.delete('/api/message/' + id);
}
export async function getMessagesByTeacher(id:number) {
    const response = await axios.get<Message[]>('/api/teacher/'+ id + '/message');
    return response.data;
}
export async function getMessagesByStudentAndTeacher(studentId: number, teacherId: number) {
    const response = await axios.get<Message[]>('/api/message/student/' +studentId+'/teacher/'+teacherId);
    return response.data;
}






