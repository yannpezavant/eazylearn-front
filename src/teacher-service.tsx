import axios from "axios";
import { Teacher } from "./entities";


export const teacherService = {
    async fetchAllTeachers(){
        const response = await axios.get<Teacher[]>("/api/teacher");
        return response.data;
    }
} 

export async function contactTeachers() {
    const response = await axios.get<Teacher[]>("/api/account/teacher");
    return response.data;
}

export async function getTeachersList(id:number) {
    const response = await axios.get<Teacher>("/api/account/teacher" + id);
    return response.data;
}

export async function teacherDetailsById(id:number) {
    const response = await axios.get(`/api/teacher/`+ id +`/details`);
    return response.data;
}

export async function getImageByTeacherId(id:number) {
    const response = await axios.get('/api/teacher/'+ id + "/image");
    return response.data;
}

export async function fetchTeacherDetails() {
    const response = await axios.get(`/api/teacher/teachers/details`);
    return response.data;
}

export async function studentsByTeacher() {
    const response = await axios.get(`/api/teacher/students`);
    return response.data;
}


