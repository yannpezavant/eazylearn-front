import '../auth/axios-config';
import '../styles/globals.css'
import type { AppProps } from 'next/app'
import Navbar from '../components/Navbar';
import { AuthContextProvider } from '../auth/auth-context';
import Footer from '../components/Footer';


export default function App({ Component, pageProps }: AppProps) {
  return (

  <>
  <AuthContextProvider>
      <Navbar/>
        <Component {...pageProps} />
      <Footer/>
  </AuthContextProvider>
  </>
  )
}
