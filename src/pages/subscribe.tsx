import React, { FormEvent, useContext } from 'react'
import { useState } from "react";
import { User } from '../entities';
import { useRouter } from 'next/router';
import { AuthContext } from '../auth/auth-context';
import { fetchUser, login, register} from '../auth/auth-service';


export default function Subscribe() {
    const router = useRouter();
    const { setToken } = useContext(AuthContext);
    const [isLogin, setIsLogin] = useState(true);
    const [error, setError] = useState("");
    const [user, setUser] = useState<User>({
        mail: '',
        password: '',
        role:'',
        lastname:'',
        name:''
    });

    function handleChange(event: any) {
        setUser({
            ...user,
            [event.target.name]: event.target.value
        });
    }


    async function handleSubmit(event: FormEvent) {
        event.preventDefault();
        console.log(user);
        
    if(isLogin){
        try {
            //fonction qui me permet de logger un user et prends en argument le mail et le mdp
            const data = await login(user.mail, user.password);
            setToken(data.token);
            //redirection vers la page account
            router.push("/account/");
    
        } catch (error: any) {
          if (error.response?.status === 401) {
            setError('Invalid login/password');
          } else {
            console.log(error);
          }
        }

    } else {
        try {
            //fonction qui me permet de register un user et prends en argument le user pour ensuite le logger et renvoyer sur la page account
            await register(user)
            const data = await login(user.mail, user.password);
            setToken(data.token);
            /*fetch les datas du nouveau user*/
            fetchUser()
          .then((userData) => {
            setUser(userData);
            router.push('/account/');
          })
          .catch((error) => {
            console.log(error);
          });
    
        } catch (error: any) {
          if (error.response?.status === 401) {
            setError('Invalid login/password');
          } else {
            console.log(error);
          }
        }
    }
    }


  return (
    <section className='flex flex-col justify-center items-center h-screen' id='teacherLanguage'>
    


{!isLogin &&
<form
    onSubmit={handleSubmit}
    method="post"
    className='contact-form flex flex-col space-y-2 w-fit mx-auto bg-[#D9D9D9] p-5 rounded-lg'>

    <div className='flex flex-col md:block md:space-x-2'>

        <input 
            className=' border rounded-md my-2 md:my-0 py-1 pl-1 '
            type="text"
            placeholder='Nom' 
            onChange={handleChange}
            name="lastName"
            
            />

        <input 
            className=' border rounded-md py-1 pl-1'
            type="text"
            placeholder='Prénom' 
            onChange={handleChange}
            name="name"
            value={user.name}
            />


    </div>

    <input 
        onChange={handleChange}
        value={user.mail}
        name="mail"
        className='border rounded-md py-1 pl-1'
        type="email"
        placeholder='Email' 
        required
        />

    <input 
        onChange={handleChange}
        value={user.password}
        name="password"
        className='border rounded-md py-1 pl-1'
        type="password"
        placeholder='Mot de passe' 
        required
        />


    <fieldset >
        <h3 className=' text-lg md:text-base  my-2' >Je m&apos;inscris comme: </h3>

            <div className="flex items-center mb-4">
                <input 
                onChange={handleChange} 
                type="radio" 
                name="role" 
                value="ROLE_TEACHER"
                className="w-4 h-4 border-gray-300 "
                checked={user.role === "ROLE_TEACHER"}
                />
                <label 
                className="block ml-2 font-medium md:text-sm">
                Enseignant
                </label>
            </div>

            <div className="flex items-center mb-4">
                <input 
                onChange={handleChange} 
                type="radio" 
                name="role" 
                value="ROLE_STUDENT" 
                className="w-4 h-4 border-gray-300 "
                checked={user.role === "ROLE_STUDENT"}
                />
                <label  
                className="block ml-2 font-medium md:text-sm ">
                Etudiant
                </label>
            </div>
    </fieldset>

    <button
            className='bg-[#DC7272] py-3 rounded-md text-white hover:text-gray-300 font-medium text-lg'
            type='submit'>
                    Valider
                        
    </button>
</form>
}

{isLogin &&
<form
    onSubmit={handleSubmit}
    method="post"
    className='contact-form flex flex-col space-y-2 w-fit mx-auto bg-[#D9D9D9] p-5 rounded-lg'>

    <input 
        onChange={handleChange}
        value={user.mail}
        name="mail"
        className=' border rounded-md py-1 pl-1'
        type="email"
        placeholder='Email' 
        required
        />

    <input 
        onChange={handleChange}
        value={user.password}
        name="password"
        className='border rounded-md py-1 pl-1'
        type="password"
        placeholder='Mot de passe' 
        required
        />

    <button
            className='bg-[#DC7272] py-3 rounded-md text-white hover:text-gray-300 font-medium text-lg'
            type='submit'>
                    Valider
                        
    </button>
</form>
}

<div className='flex w-full justify-center mb-10'>

        <button
                className='bg-[#DC7272] p-3 rounded-md text-white hover:text-gray-300 font-medium text-lg mt-10
                hover:border hover:border-gray-600'
                type='submit'
                onClick={() => setIsLogin(!isLogin)}>{isLogin ? 'M\'inscrire':'Me logger'}</button>
    </div>

    </section>
  )
}




