import DashboardNav from '@/src/components/DashboardNav'
import TeacherCard from '@/src/components/TeacherCard';
import TeacherContact from '@/src/components/TeacherContact'
import { Message, Student, Teacher, User } from '@/src/entities';
import { useRouter } from 'next/router';
import React, { useContext, useEffect, useState } from 'react'
import { getTeachersList, studentsByTeacher, teacherDetailsById } from '@/src/teacher-service';
import { GetServerSideProps } from 'next';
import ListStudent from '@/src/components/ListStudent';
import { fetchUser } from '@/src/auth/auth-service';
import { deleteMessage, getMessagesByStudentAndTeacher, postMessage } from '@/src/message-service';
import FormMessage from '@/src/components/FormMessage';


interface Props{
    teachers:Teacher;
    messages:Message[];
}

export default function Lessons({}:Props) {
    const [user, setUser] = useState<User>();
    const [studentId, setstudentId] = useState<number>();
    const [error, setError] = useState(""); 
    const [messages, setMessages] = useState<Message[]>([]);
    const router = useRouter();
    const { id } = router.query;

    
    console.log(messages);


    useEffect(() => {
        fetchUser().then(data => {
            setUser(data);
        }).catch(error => {
            if(error.response.status == 401) {
              setError("User not found");
            }
        });
      }, [])

      
      useEffect(() => {
        // console.log(user,studentId);
        if(!user?.id || !studentId ) return;
        const fetchMessages = async () => {
          try {
            const message = await getMessagesByStudentAndTeacher(studentId, user.id!);
            setMessages(message);
          } catch (error) {
            // Gérer l'erreur si nécessaire
            console.error('Erreur de récupération:', error);
          }
        };
    
        fetchMessages();
      }, [user?.id, studentId]);


      async function addMessage(message:Message) {
            // console.log(message);
            message.idStudent = Number(studentId);
            message.idTeacher = Number(user?.id);
        try {

        const response = await postMessage(message);
        const updatedMessages = [...messages, response]; // Ajoute le message au useState messages
                setMessages(updatedMessages);
                setError('');
        
        } catch(error:any) {
            if(error.response.status == 400) {
                setError(error.response.data.detail);
            }
        }
    }

    // const handleDeleteMessage = (deletedMessageId: number) => {
    //     /* supprime un message spécifique du tableau messages en le filtrant à l'aide de son identifiant. */ 
    //     const updatedMessages = messages.filter(message => message.id !== deletedMessageId);
    //     setMessages(updatedMessages);
    // };

    const handleDeleteMessage = async (deletedMessageId: number) => {
        try {
          // Envoyer une requête pour supprimer le message par son ID
          await deleteMessage(deletedMessageId);
          // Mettre à jour la liste des messages localement en filtrant le message supprimé
          const updatedMessages = messages.filter((message) => message.id !== deletedMessageId);
          setMessages(updatedMessages);
          setError('');
        } catch (error) {
          console.error('Erreur lors de la suppression du message :', error);
          setError('Erreur lors de la suppression du message.');
        }
      };
      


  return (
    <>
    <DashboardNav/>
    <div className='flex h-screen'>
        {/*SECTION PROFS*/}
        <section className='w-1/4 '> 
        {user?.role === "ROLE_TEACHER" ?

            <h2 className='flex justify-center py-5 font-semibold text-lg' >
                mes élèves</h2>
         : 
            <h2 className='flex justify-center py-5 font-semibold text-lg' >
                mes enseignants</h2>
        }

            {/*liste profs/élèves*/}
            <div className='flex flex-col justify-between'>
                <div className=''>
                    <ListStudent onSelectStudent={(studentId) => setstudentId(studentId)}/> 
                </div>
            </div>
        </section>


        {/*SECTION DIALOGUE*/}
        <section className='w-3/4 border-x-2 pt-4'>
            <div className='flex flex-col justify-between '>

                {/*liste dialogue*/}
                <div className='m-2'>
                    {messages.map((item) => (
                        <div 
                        className='flex justify-between  rounded-2xl w-full bg-gray-100 mb-2 p-2'
                        key={item.id} >
                        <div  
                        className='text-md '
                        id='teacherLanguage'
                        >{item.content}</div>

                        <button
                            /* Appel de la fonction de suppression lors du clic sur le bouton*/
                            onClick={() => handleDeleteMessage(item.id!)} 
                            className='bg-[#DC7272] text-white font-light text-base rounded-xl mr-1 px-2'
                        >
                            Supprimer
                        </button>
                        </div>
                    ))}
                </div>
                
                <div className='mt-10 mb-2 p-2 '>
                    <FormMessage onSubmit={addMessage}/>
                </div>
            </div>
        </section>
    </div>
    </>
  )
}



