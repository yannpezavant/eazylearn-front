/* eslint-disable @next/next/no-img-element */
import DashboardNav from '@/src/components/DashboardNav'
import React, { useContext, useEffect, useState } from 'react'
import { Student, User } from '@/src/entities';
import { AuthContext } from '@/src/auth/auth-context';
import { useRouter } from 'next/router';
import { fetchUser } from '@/src/auth/auth-service';




export default function Index() {
  const [user, setUser] = useState<User>();
  const [student, setStudent] = useState<Student>();

  const [error, setError] = useState("");
  const { setToken} = useContext(AuthContext); 
  const router = useRouter();
  const { id } = router.query;
  console.log(user);
  
  


/*fetchUser */
  useEffect(() => {
    fetchUser()
    .then(data => {
        setUser(data);
    }).catch(error => {
        if(error.response.status == 401) {
          setError("Invalid credentials");
        }
    });
  }, [])

/*fecthStudent */
  // useEffect(() => {
  //   fetchStudent()
  //   .then(data => {
  //       setUser(data);
  //   }).catch(error => {
  //       if(error.response.status == 401) {
  //         setError("Invalid credentials");
  //       }
  //   });
  // }, [])









  const handleLogout = () => {
    setToken(null);
    router.push('/'); // Redirige vers la page d'accueil après la déconnexion
  };
  

  console.log(user);

  return (
    <>
    <DashboardNav/>

    <div className='flex flex-col md:flex-row justify-center items-center h-[41rem] md:h-[33rem] '>

        <div className='flex justify-center pt-10 md:pt-0 my-auto '>
              <div className='rounded-xl overflow-hidden'>
                <img src={user?.image} 
                className='rounded-full md:rounded-xl w-60 h-60 md:w-40  object-cover hover:scale-110 transition-all duration-500 '
                alt="" />

              </div>

        </div>


      {/*FORM*/}

        <div className='w-full md:w-1/2 h-full flex justify-center items-center '>

                <form 
                action="/send-data-here" 
                method="post"
                className='w-96 '
                >
            
                  <div className='flex my-2'>
                  
                        <label 
                          htmlFor="name"
                          className='w-1/4 text-normal md:text-sm flex items-center font-medium'
                          >
                          Nom
                        </label>
                        <input 
                          className=' border border-black/50 focus:border-black rounded-xl py-1 pl-1 w-3/4'
                          type="text"
                        />
                  </div>

                  <div className='flex justify-between my-2'>

                        <label 
                          htmlFor="lastName"
                          className='w-1/4 text-sm flex items-center font-medium text-normal md:text-sm'
                          >
                          Prenom
                        </label>

                        <input 
                          className=' border border-black/50 focus:border-black rounded-xl py-1 pl-1 w-3/4'
                          type="text" 
                        />
                  </div>

                  <div className='flex justify-between my-2'>

                        <label 
                          htmlFor="mail"
                          className='w-1/4 text-sm flex items-center font-medium text-normal md:text-sm'
                          >
                          Email
                        </label>

                        <input 
                          className=' border border-black/50 focus:border-black rounded-xl py-1 pl-1 w-3/4'
                          type="email" 
                          placeholder={user?.mail}
                        />
                  </div>

                  <div className='flex  my-2 '>

                        <label 
                          htmlFor="password"
                          className='w-1/4 text-sm flex items-center font-medium text-normal md:text-sm'
                          >
                          Mot de passe
                        </label>

                        <input 
                          className=' border border-black/50 focus:border-black rounded-xl py-1 pl-1 w-3/4'
                          type="password" 
                          
                        />
                  </div>

                  {/* <div className='flex justify-end my-5 ml-20  '>
                    <button
                        className='bg-[#FFF1F1] rounded-full text-[#DC7272] hover:text-[#FFF1F1] hover:bg-[#DC7272] font-medium text-sm py-2 px-4'
                        type='submit'>
                    modifier  mes coordonnées          
                  </button>
                </div> */}


                </form>
          </div>
          
        </div>


{/*DECONNECTION*/}
      <div className='flex justify-center my-5 '>
        <button className='bg-[#DC7272] rounded-full text-[#FFF1F1] hover:text-[#DC7272] hover:bg-[#FFF1F1] font-medium text-sm py-2 px-4'>
          <a className="item" onClick={handleLogout}>
            Déconnexion
          </a>
        </button>

      </div>
    </>
  )
}






