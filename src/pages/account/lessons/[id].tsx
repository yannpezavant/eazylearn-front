/* eslint-disable @next/next/no-img-element */
import FormMessage from '@/src/components/FormMessage';
import { Language, Message, Teacher, User } from '@/src/entities';
import { getMessagesByStudentAndTeacher, getMessagesByTeacher } from '@/src/message-service';
import { getTeachersList, teacherDetailsById } from '@/src/teacher-service';
import { GetServerSideProps } from 'next';
import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'
import {postMessage} from '@/src/message-service';
import ListMessages from '@/src/components/ListMessages';
import TeacherCard from '@/src/components/TeacherCard';
import DashboardNav from '@/src/components/DashboardNav';
import { fetchUser } from '@/src/auth/auth-service';

interface Props{
    teachers:Teacher;
    messages:Message[];
}

export default function Id({teachers, messages:msgProps}:Props) {
    const [messages,setMessages] = useState(msgProps);
    const [user, setUser] = useState<User>();
    const [error, setError] = useState("");
    const router = useRouter();
    const { id } = router.query;
    console.log(messages);


    const handleDeleteMessage = (deletedMessageId: number) => {
        /* supprime un message spécifique du tableau messages en le filtrant à l'aide de son identifiant. */ 
        const updatedMessages = messages.filter(message => message.id !== deletedMessageId);
        setMessages(updatedMessages);
    };

    async function addMessage(message:Message) {
        console.log(message);
        message.idTeacher = Number(id);
    try {
      const response = await postMessage(message);
      const updatedMessages = [...messages, response]; // Ajoute le nouveau message à la liste
            setMessages(updatedMessages);
            setError('');
        
    } catch(error:any) {
        if(error.response.status == 400) {
            setError(error.response.data.detail);
        }
    }
  }


  
  return (
    <>
    <div className='flex   min-h-screen '>

{/*TEACHER DETAILS*/}

<section className='border-r-2 w-1/4'>

        <div className='flex flex-col md:flex-row  md:w-80  rounded-md mx-2 md:mx-auto mt-2 p-2 border'>
                <div className=' rounded-xl my-auto mx-auto overflow-hidden md:w-1/4 justify-center'>
                      <img 
                      src={teachers.image} 
                      alt={`Image of ${teachers.name}`} 
                      className=' rounded-full w-20 h-20 object-cover hover:scale-125 transition-all duration-500'
                      />

                </div>

                    <div className='flex flex-col md:w-3/4 '>

                        <div className='flex flex-col md:flex-row justify-center items-center h-3/4 w-full text-2xl font-medium  my-3 md:my-0' 
                        id='teacherId'>
                            <h2 className='md:mr-4'>{teachers.name}</h2>
                            <h2>{teachers.lastName}</h2>
                        </div >

                        <div className='flex justify-center items-center h-1/4 mb-4 rounded-3xl md:rounded-full md:mx-8 bg-[#FFF1F1] ' id='teacherLanguage'>
                            <p className='text-[#DC7272] py-6 '>{teachers.language}</p>
                        </div>
                    </div>
            
        </div>
</section>

<section className='w-3/4 flex flex-col px-4'>
<h3 className="mb-5 text-lg" id='teacherId'>Messages</h3>

    <div className='flex flex-col  h-screen'>

<div className=''>
        {messages.map(item =>
            <ListMessages key={item.id} message={item} onMessageDelete={handleDeleteMessage} />
        )}
</div>

        <div className="mt-auto">
            <FormMessage onSubmit={addMessage}/>
        </div>
    </div>
</section>
    </div>
    
    </>
  )
}


export const getServerSideProps: GetServerSideProps<Props> = async (context) => {
    const id = context.query.id 

    try {
    return {
        props: {
            teachers: await teacherDetailsById(Number(id)),
            messages: await getMessagesByStudentAndTeacher(Number(id), Number(id))
        }
    }
}
catch {
    return {
        notFound: true
    }
}


}







