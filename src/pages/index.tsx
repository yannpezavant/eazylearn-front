import Image from 'next/image'
import { Inter } from 'next/font/google'
import { Language, Teacher } from '../entities';
import { GetServerSideProps } from 'next';
import { languageService } from '../language-service';
import React from 'react';
import Section1 from '../components/Section1';
import Section2 from '../components/Section2';
import Section3 from '../components/Section3';


const inter = Inter({ subsets: ['latin'] })

interface Props{
  languages: Language[];
}

export default function Home({languages}:Props) {
  
  return (
    <>
      <Section1 languages={languages}/>
      <Section2/>
      <Section3/>
    </>
  )
}

export const getServerSideProps: GetServerSideProps<Props> = async () => {
const languages = await languageService.fetchAllLanguages();


  return {
    props: {
      languages
    }
  }

}


