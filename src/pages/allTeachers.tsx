/* eslint-disable @next/next/no-img-element */
import React, { useContext, useEffect, useState} from 'react'
import { fetchTeacherDetails } from '../teacher-service';
import { useRouter } from 'next/router';
import { Language, Teacher, User } from '../entities';
import { AuthContext } from '../auth/auth-context';
import { GetServerSideProps } from 'next';

interface TeacherDetail {
    id:string;
    name: string;
    lastName: string;
    image: string;
    language: string;
  }



export default function AllTeachers() {
    const [teacherDetails, setTeacherDetails] = useState<TeacherDetail[]>([]);
    const [user, setUser] = useState<User>();
    
    


  useEffect(() => {
    fetchTeacherDetails()
      .then(data => {
        setTeacherDetails(data);
      })
      .catch(error => {
        // Gérer l'erreur si nécessaire
        console.error('Erreur de récupération:', error);
      });
  }, []);

  

  return (
    <div>
        <div className=' md:h-screen px-10 bg-[#FFF1F1] bg-gradient-to-b from-transparent to-white mb-5'>
            <h1 className='flex justify-start items-center w-full p-10 text-2xl font-semibold text-center' id='titleTeacherList'>Les professeurs de toute matières</h1>
            
            <div className='grid grid-cols-1 md:grid-cols-3 gap-4 md:gap-10 md:mx-10'>
                {teacherDetails.map((teacher, index) => (
                <div 
                key={index}
                className='flex w-96 h-60 rounded-xl shadow-md border bg-white' 
                >
                    <div className=' w-1/2 rounded-xl my-auto overflow-hidden'>
                      <img 
                      src={teacher.image} 
                      alt={`Image of ${teacher.name}`} 
                      className=' rounded-xl w-40 h-60 object-cover hover:scale-125 transition-all duration-500'
                      />

                    </div>

                      <div className='flex flex-col w-1/2'>
                          <div className='flex flex-col justify-center items-center  h-2/3 w-full text-2xl font-medium' 
                          id='teacherId'>
                              <h2 >{teacher.name}</h2>
                              <h2>{teacher.lastName}</h2>
                          </div >
                          <div className='flex justify-center items-center h-1/3 mb-4 rounded-full mx-8 bg-[#FFF1F1]' id='teacherLanguage'>
                              <p className='text-[#DC7272]'>{teacher.language}</p>
                      </div>   

                        
                    </div>
                </div>
                ))}
            </div>
    </div>
      

    
    </div>
  )
}


