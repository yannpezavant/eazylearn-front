/* eslint-disable react-hooks/rules-of-hooks */
import TeacherCard from '@/src/components/TeacherCard'
import { Language, Teacher } from '@/src/entities'
import { fetchTeacherByLanguages, languageService } from '@/src/language-service'
import { GetServerSideProps } from 'next'
import { useRouter } from 'next/router'
import React from 'react'

interface Props{
    teachers:Teacher[];
    language:Language[];
}

export default function id({teachers, language}:Props) {
    
    const router = useRouter();
    const { id } = router.query;
    console.log(teachers);

  return (
    <>
    <div className='flex flex-col justify-start items-center p-10 bg-[#FFF1F1] bg-gradient-to-b from-transparent to-white h-screen'>
        <h1 >Vous avez choisi:</h1>
        <div className='flex justify-center font-medium text-lg my-2 ' id='titleTeacherList'>
            
          {language.map((item) => (
              <p key={item.id}>{item.name}</p>
            ))}
          </div>
          
        <div>
        </div>
        <div className='grid gap-4 row-gap-5 grid-cols-1 lg:grid-cols-2 mt-8'>
        {teachers.map((item) => (
            <TeacherCard key={item.id} teachers={item} />
        ))}
        </div>

    </div>
    </>

  )
}


export const getServerSideProps: GetServerSideProps<Props> = async (context) => {
    const id = context.query.id

    try {
    return {
        props: {
            teachers: await fetchTeacherByLanguages(Number(id)),
            language: await languageService.fetchLanguageByTeacher(Number(id))
        }
    }
}
catch {
    return {
        notFound: true
    }
}

}






