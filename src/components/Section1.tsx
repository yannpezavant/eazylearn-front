import React from 'react'
import LanguageDropdown from './LanguageDropdown'
import { Language } from '../entities';
import Image from 'next/image';

interface Props{
    languages: Language[];
  }
  
export default function Section1({languages}:Props) {
  return (
    <section className='flex bg-[#FFF1F1] h-screen md:h-96 '>
        
          <div className='flex flex-col  justify-around md:w-3/5 md:px-12 '>
                <div className='relative -top-24 md:top-0 md:p-5'>
                    <h2 className='flex font-semibold text-lg justify-center md:justify-start  ' id='titleTeacherList'>
                      Trouvez le professeur parfait</h2>
                    <p className=' text-center md:text-start text-[#868686] text-lg font-normal mt-5 mx-10 ' 
                    id='teacherLanguage'>
                      En ligne ou en face à face, 
                    <br/> faites votre choix parmis notre liste de professeurs.</p>

                </div>

                <div className='relative p-5 mx-8 md:ml-0 mt-10 md:mt-0 -top-96 md:top-0 '>
                    <h2 className='font-semibold text-base md:text-xl mb-5' id='titleTeacherList'>
                      Quelle langue souhaitez-vous apprendre ?</h2>
                      <LanguageDropdown languages={languages} />
                </div>

          </div>

          <div className='absolute flex justify-center h-96 top-[30rem]  md:top-20  left-[4rem] md:left-[35rem] lg:left-[60rem]'>

              
                <div className="relative w-24 md:w-20 h-64  top-28 md:top-0 ">
                    <div className="absolute flex bg-gray-500 rounded-t-full md:rounded-full w-24 md:w-20 h-80 md:h-96 justify-center items-center md:my-4 ">
                      <Image 
                      src="/img1.jpg" 
                      alt="img1" 
                      fill
                      style={{
                          objectFit: 'cover'
                        }}
                      className='rounded-t-full md:rounded-full ' 
                      />
                    </div>
                </div>

                
                  <div className=" relative  w-24 md:w-20 h-64 md:pt-10 mx-4 top-12 md:top-0 ">
                      <div className="absolute flex bg-gray-500 rounded-t-full md:rounded-full w-24 md:w-20 h-96 md:h-96 justify-center items-center md:my-4">
                      <Image 
                      src="/img2.jpg" 
                      alt="img2" 
                      fill
                      style={{
                          objectFit: 'cover',
                        }} 
                      className='rounded-t-full md:rounded-full' 
                      />
                      </div>
                  </div>

                
                  <div className=" relative w-24 md:w-20 h-64 md:pt-20 top-28 md:top-0 ">
                      <div className="absolute flex bg-gray-500 rounded-t-full md:rounded-full w-24 md:w-20 h-80 md:h-96 justify-center items-center md:my-4 ">
                      <Image 
                      src="/img3.jpg" 
                      alt="img3" 
                      fill
                      style={{
                          objectFit: 'cover',
                        }} 
                      className='rounded-t-full md:rounded-full' 
                      />
                      </div>
                  </div>
          </div>

    </section>
  )
}
