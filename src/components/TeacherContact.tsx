/* eslint-disable @next/next/no-img-element */
import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { teacherDetailsById } from '@/src/teacher-service';
import TeacherCard from '@/src/components/TeacherCard';
import { Teacher } from '../entities';



export default function Id() {
    const [error, setError] = useState("");
    const router = useRouter();
    const { id } = router.query;
    const [teacherDetails, setTeacherDetails] = useState<Teacher>();
    console.log(teacherDetails);
    

    useEffect(() => {
        const fetchTeacherDetails = async () => {
            try {
                const response = await teacherDetailsById(Number(id));
                setTeacherDetails(response);
            } catch (error) {
                setError("Teacher not found");
            }
        };

        if (id) {
            fetchTeacherDetails();
        }
    }, [id]);

    return (
        <>
            <div className='flex flex-col p-10 min-h-screen border border-green-500'>
                {/*TEACHER DETAILS*/}
                <div className='flex justify-center w-80 mx-auto rounded-md p-2 border border-green-500'>
                    <div className=' rounded-xl my-auto mx-auto overflow-hidden w-1/4 justify-center'>
                        <img
                            src={teacherDetails?.image}
                            alt={`Image of ${teacherDetails?.name}`}
                            className=' rounded-full w-20 h-20 object-cover hover:scale-125 transition-all duration-500'
                        />
                    </div>
                    {/* Rest of your code */}
                    <div className='flex justify-center items-center h-3/4 w-full text-2xl font-medium  ' 
                        >
                            <p>{teacherDetails?.name}</p>
                            <p>{teacherDetails?.lastName}</p>
                        </div >
                </div>
            </div>
        </>
    )
}


