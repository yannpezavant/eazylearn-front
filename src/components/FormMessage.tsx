import React, { FormEvent, useState } from 'react'
import { Message } from '../entities';
import {postMessage} from '@/src/message-service';

/*event*/
interface Props {
  onSubmit:(message:Message) => void;
  message?: Message;
}


export default function FormMessage({message:initialMsg,onSubmit}:Props) {
  const [errors, setErrors] = useState('');
  const [message, setMessage] = useState<Message>(initialMsg? initialMsg:{
        content: ""
  });


  function handleChange(event: any) {
    setMessage({
        ...message,
        [event.target.name]: event.target.value
    });
}

  async function handleSubmit(event:FormEvent) {
    event.preventDefault();
    try {
        onSubmit({...message});
        message.content=""
    } catch(error:any) {
        if(error.response.status == 400) {
            setErrors(error.response.data.detail);
        }
    }
}

  return (
    <div>
        <form 
                    onSubmit={handleSubmit}
                    method="post"
                    className='flex flex-col my-2'
                    >
                    {errors && <p>{errors}</p>}
                    
                    <input 
                        placeholder='Votre message'  
                        type="text" 
                        name="content" 
                        className="border border-gray-500 rounded form-control" 
                        value={message.content}
                        onChange={handleChange} 
                        required
                    />
                    
                    <button 
                    className='bg-[#DC7272] px-3 py-1 rounded-full text-white hover:text-gray-300 font-medium text-sm mt-2 w-28'
                    type='submit'
                    >
                        Envoyer
                        </button>
                    </form>
    </div>
  )
}

