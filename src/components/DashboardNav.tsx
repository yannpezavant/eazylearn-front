import Link from 'next/link'
import React from 'react'
import { Teacher } from '../entities';

interface Props {
  teachers: Teacher;
}

export default function dashboardNav() {
  return (
    <>
    
    <div className='flex justify-center items-center left-0 right-0 top-0 bg-[#D9D9D9] h-12 text-white font-semibold text-lg' id='teacherLanguage'>
  <Link href={`/account`} className="">
    <div className='mr-5 hover:underline hover:underline-white transition-all '>
      Mon Compte
    </div>
  </Link>

  <Link href={`/account/lessons/`} className="">
    <div className='ml-5 hover:underline hover:underline-white transition-all '>Mes Cours
    </div>
  </Link>
</div>

</>

  )
}
