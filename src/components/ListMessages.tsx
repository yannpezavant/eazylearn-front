import { AuthContext } from "../auth/auth-context"; 
import router, { useRouter } from "next/router";
import React, { useContext, useState } from "react";
import { Message } from "../entities";
import { deleteMessage, updateMessage } from "../message-service";
import FormMessage from "./FormMessage";

interface Props {
    message: Message;
    onMessageDelete: (messageId: any) => void;
}


export default function ListMessages({ message:initial, onMessageDelete}: Props) {
    const [message, setMessage] = useState(initial)
    const [isDeleting, setIsDeleting] = useState(false);
    const [isEditing, setIsEditing] = useState(false);
    const [showEdit, setShowEdit] = useState(false);

    const [messageUpdate, setMessageUpdate] = useState<Message>();

    const authContext = useContext(AuthContext); // Utilisez le contexte d'authentification si nécessaire
    const router = useRouter();


    /*fonction de suppression de message*/
    const handleDelete = async () => {
        try {
            setIsDeleting(true); // Mise à jour de l'état pour indiquer que la suppression est en cours
            await deleteMessage(message.id); // Appel de la fonction de suppression du service
            setIsDeleting(false); // Mise à jour l'état pour indiquer que la suppression est terminée
            //verification que l'id du message est bien récupéré
            console.log(message.id);
            // Mise à jour de l'interface utilisateur, en supprimant le message de l'affichage
            onMessageDelete(message.id);
            
        } catch (error) {
            console.error("Erreur lors de la suppression du message", error);
            setIsDeleting(false); // En cas d'erreur, remise à jour de l'état
        }
    };

    async function toggleEdit() {
        setShowEdit(!showEdit);
    }

    const handleUpdate = async (messageId: any) => {
        try {
            const updatedMessage = {
                ...messageId
            };
            await updateMessage(updatedMessage);
            setShowEdit(false);
            setMessage(updatedMessage);
            // console.log(updatedMessage);
        } catch (error) {
            console.error("Erreur lors de la mise à jour du message", error);
        }
    };

    
    //Formatte la date a afficher avec le toLocaleDateString
    let formattedDate = ""; // Initialisez la variable en dehors de la condition

    if (message.date) {
        const messageDate = new Date(message.date);
        formattedDate = messageDate.toLocaleDateString();
    }
    
    return (
        <>
            <div className="flex flex-col  rounded w-full border mb-2 p-2">
                <h5 className="w-full" id='teacherLanguage'>{message.content}</h5>
            {message.date && (
                <p className="flex justify-end my-2" id='teacherLanguage'>{formattedDate}</p>
                    
            )}
                <div className="text-sm my-1 md:my-0">
                <div className="flex items-center justify-end ">
                    <button 
                    onClick={handleDelete} 
                    disabled={isDeleting}
                    className="bg-[#DC7272] text-white font-light text-sm rounded-xl mr-1 px-2" 
                    id='teacherLanguage'
                    >
                        {isDeleting ? "Suppression en cours..." : "Supprimer"}
                    </button>
                    <button className="bg-[#DC7272] text-white font-light text-sm  rounded-md  px-2"
                    id='teacherLanguage'
                    onClick={toggleEdit}>Editer</button>
                </div>
                </div>
                </div>
            <div>
            {showEdit &&
                <>
                    <h2 className="bg-[#DC7272] text-white font-light text-sm w-28 rounded-md mb-2 p-2" id='teacherLanguage'>Edit Message</h2>
                    <div className="edit">
                        <FormMessage message={message}
                            onSubmit={handleUpdate} />
                    </div>
                </>
            }
            </div>

        </>
    )
            
}