/* eslint-disable @next/next/no-img-element */
import React, { useContext, useEffect, useState} from 'react'
import Image from 'next/image';
import { Language, Teacher, User } from '../entities';
import { AuthContext } from '../auth/auth-context';
import { useRouter } from 'next/router';
import { getImageByTeacherId } from '../teacher-service';
import { fetchUser } from '../auth/auth-service';

interface Props{
  teachers:Teacher;
}


export default function TeacherCard({ teachers}:Props) {
  const { token } = useContext(AuthContext);
  const [user, setUser] = useState<User>();
  const [error, setError] = useState("");

  const [teacherImage, setTeacherImage] = useState<string | null>(null);
  
  const router = useRouter();
  const { id } = router.query;
  const teacherId = teachers.id; /*recupere l'id du teacher*/

  
    const handleContactClick = () => {
      console.log(token);
      
      if (token && user?.role === 'ROLE_STUDENT') {
        // Rediriger vers la page account/lessons si authentifié
        window.location.href = `/account/lessons/${teachers.id}`;
  
      } else {
        alert("Vous devez être inscrit pour contacter un professeur.");
        window.location.href = `/`;
      }
    };


  useEffect(() => {
    fetchUser().then(data => {
        setUser(data);
    }).catch(error => {
        if(error.response.status == 401) {
          setError("User not found");
        }
    });
  }, [])


  useEffect(() => {
    getImageByTeacherId(Number(teacherId))
      .then(imageUrl => {
        setTeacherImage(imageUrl);
        console.log(teacherId);
      })
      .catch(error => {
        console.error('Erreur de récupération:', error);
      });
  }, [teacherId]);





  return (
    <div className='flex rounded-2xl w-96 h-60 shadow-md border '>

        <div className='flex justify-center overflow-hidden rounded-xl w-1/2 '>
          {teacherImage ? (
            <img src={teacherImage} 
            alt="teacherImage" 
            className='rounded-xl h-60 object-cover hover:scale-125 transition-all duration-500 w-full'  />
          ) : (
            <div>Loading...</div>
          )}
        </div>

        
    <div className='flex flex-col w-1/2 bg-white rounded-r-xl'>

        <div className='flex flex-col justify-center items-center  h-2/3 w-full text-2xl font-medium'>
                        <div className='flex flex-col justify-center items-center  text-2xl font-medium' 
                        id='teacherId'>
                            <h2 >{teachers.name}</h2>
                            <h2 >{teachers.lastName}</h2>
                        </div >
                    </div>
          

           <div className='flex justify-center items-center h-1/3 mb-4 rounded-full mx-8 bg-[#FFF1F1] hover:bg-[#DC7272] 
           text-[#DC7272] hover:text-[#FFF1F1] ease-in-out'>

                <button 
                id='teacherLanguage'
                onClick={handleContactClick}
                >
                Contacter
                </button>
            </div>
      </div>

        

    </div>
  )
}
