import React, { useState } from 'react';
import { Language } from '../entities';

interface Props {
  languages: Language[];
}

export default function LanguageDropdown({ languages }: Props) {
  const [selectedLanguage, setSelectedLanguage] = useState('Rechercher');
  const [openLanguages, setOpenLanguages] = useState(false);


  const handleLanguageClick = (language: string) => {
    setSelectedLanguage(language);
    setOpenLanguages(false);
  };


  return (
    <div onMouseLeave={() => setOpenLanguages(false)} className="relative">
      <button
        onClick={() => setOpenLanguages(true)}
        className="flex items-center justify-center p-2 py-2  w-40  bg-[#DC7272] text-white hover:bg-[#DC7272]/80 font-normal"
        id='teacherLanguage'
      >
        <p>{selectedLanguage}</p>
      </button>

      <ul
        className={`absolute left-40 -top-0 w-60 ${openLanguages ? 'block' : 'hidden'}` } id='teacherLanguage'
      >
        {languages.map((language:any) => (
          <li
            key={language.id}
            onClick={() => handleLanguageClick(language.name)}
            className="cursor-pointer flex items-center px-3 py-2  w-40  bg-[#DC7272] text-white font-normal text-sm hover:bg-[#DC7272]/50 hover:text-[#DC7272]" 
          >
            <a href={`/teacherByLanguage/${language.id}`}>
              {language.name}
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
}



