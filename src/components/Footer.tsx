import React from 'react'

export default function Footer() {
  return (
    <footer className='bottom-0 left-0 right-0 md:flex bg-black text-white text-sm font-thin justify-around items-center p-4 ' id='teacherLanguage'>
        <div className='flex flex-col my-5'>
            <p>Nos valeurs</p>
            <p>Notre engagement</p>
            <p>Contact</p>
        </div>
        <div className='my-5'>
            <p>Langue</p>
            <p>Pédagogie</p>
        </div>
        <div className='my-5'>
            <p className='mb-2'>Suivez-nous</p>
            <div className='flex md:justify-around '>
                <div className='rounded-full w-4 h-4 bg-white mr-2'></div>
                <div className='rounded-full w-4 h-4 bg-white mx-2'></div>
                <div className='rounded-full w-4 h-4 bg-white mx-2'></div>
                <div className='rounded-full w-4 h-4 bg-white ml-2'></div>
            </div>
        </div>
        <div className='my-5'>
            <p>Mentions légales</p>
            <p>Confidentialité</p>
        </div>
    </footer>
  )
}

