import Link from 'next/link'
import React from 'react'

export default function Section2() {
  return (
    <section className='flex justify-start items-center bg-[#FFF1F1] h-96 bg-gradient-to-b from-transparent to-white'> 
        <div className='mx-8 md:ml-16 ' >
            <h2 className='flex justify-center md:justify-start font-semibold text-xl ' id='titleTeacherList'>
              Tous nos professeurs sont certifiés !</h2>
            <p className=' text-[#868686] text-lg font-normal my-10 md:ml-10 text-center md:text-start ' id='teacherLanguage'>
            Plongez dans une expérience d&apos;apprentissage des langues étrangères exceptionnelle <br/>grâce à notre équipe d&apos;enseignants qualifiés et expérimentés!</p>
            <Link
              href={`/allTeachers`}
              className="flex justify-center md:justify-start"
            >
              <button className=' px-5 py-2 rounded-full bg-[#DC7272] text-white font-normal hover:bg-[#DC7272]/80' id='teacherLanguage'>
                  Trouvez votre professeur
              </button>
            </Link>
        </div>

    </section>
  )
}

