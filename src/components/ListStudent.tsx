/* eslint-disable @next/next/no-img-element */
import React, { useContext, useEffect, useState } from 'react'
import { Student, Teacher } from '../entities';
import { useRouter } from 'next/router';
import { studentsByTeacher, teacherDetailsById } from '../teacher-service';

interface Props {
    onSelectStudent: (studentId: number) => void;
  }



export default function ListStudent({ onSelectStudent }: Props) {

  const [newStudent, setNewStudent] = useState<Student[]>([]);
  const [error, setError] = useState("");
  const router = useRouter();
  const { id } = router.query;
  const [selectedStudentId, setSelectedStudentId] = useState<number>();
  console.log(newStudent);
  

useEffect(() => {
    const fetchStudentDetails = async () => {
        try {
            const response = await studentsByTeacher();
            setNewStudent(response);
        } catch (error) {
            setError("Student not found");
        }
    };
        fetchStudentDetails();
}, []);


/*event qui recupere l'id*/
const handleStudentSelect = (studentId: any) => {
    setSelectedStudentId(studentId);
    // fonction de remontée vers le parent avec l'ID de l'étudiant
    onSelectStudent(studentId);
  };

  return (
    <>
    <div className=' '>

    {newStudent.map(item =>
            <div
            key={item.id}>
         <div className='flex flex-col md:w-40 mx-auto rounded-lg mb-2 border border-gray-400 md:border-gray-200 hover:border-gray-400'>
           <button 
           className=''
           onClick={() => handleStudentSelect(item.idUser)}>
            <div className='flex flex-col justify-center items-center text-xl font-semibold md:font-normal bg-gray-100 rounded-lg' 
            id='teacherId'>
                <h2 >{item.name}</h2>
                <h2>{item.lastName}</h2>
            </div >
                </button>
            
            </div>          
        </div>
                
        )}
    </div>
    </>
  )
}


