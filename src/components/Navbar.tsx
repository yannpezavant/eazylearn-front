import Link from 'next/link'
import React, { useContext } from 'react'
import { IconAccount } from '../svg'
import { AuthContext } from '../auth/auth-context';

export default function Navbar() {

  const { token } = useContext(AuthContext);


  return (
    
      <nav className='left-0 right-0 top-0 bg-[#FFF1F1] '>
        <div className='flex items-center justify-between p-4 '>

              <Link
                  href={`/`}
                  className=""
                >
                <div className=' uppercase font-semibold text-xl sm:text-2xl text-[#DC7272] md:ml-8 ' id='logo'>
                    eazy&nbsp;learn
                </div>
              </Link>
          

              {/* Connection */}
          <div>

            <button type="button" className=" justify-center items-center py-2 px-4 text-base font-normal text-center text-[#868686]">
            {token ? (
            <Link href={`/account/`}>
              <IconAccount
                            className="w-7 h-7 fill-[#DC7272]"
                        />
            </Link>
            ):(
              <Link href={`/subscribe`}>
              <IconAccount
                            className="w-7 h-7 fill-[#DC7272]"
                        />
            </Link>
            )}
            
            </button>

          </div>

        </div>
      </nav>
  )
}
