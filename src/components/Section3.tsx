import React from 'react'
import { Calendrier, Loupe, Telephone} from '../svg'

export default function Section3() {
  return (
    <section className='flex flex-col justify-start items-center'>

        <h1 className='text-[#E14C4C] tracking-widest my-10 font-bold text-lg md:text-2xl ' id='titleTeacherList'>Apprenez en toute simplicité</h1>

        <div className='flex flex-col items-center w-4/5 '> 

            <div className='flex w-full md:w-auto py-4 '>
                <div className='flex flex-col justify-center w-80 mr-20 '>
                    <h2 className='text-[#E14C4C] text-md md:text-xl font-semibold ' id='titleTeacherList'>1.&nbsp;Recherchez</h2>
                    <p className=' font-medium  text-sm md:text-base text-[#868686] ' id='teacherLanguage'>Consultez librement les profils selon vos critères.</p>
                </div>

                <div className='bg-[#7EA3EC]/50 w-40 md:w-80 h-32 md:h-52 rounded-[48px] '>
                    
                <div className='flex justify-center items-center w-40 md:w-80 h-32 md:h-52  '>

                    <Loupe
                        className="w-10 md:w-20 h-10 md:h-20 "
                    />
                </div>
                    
                </div>
            </div>


            <div className='flex w-full md:w-auto  py-4 '>
                <div className='bg-[#FFF1F1]/50 w-40 md:w-80 h-32 md:h-52 rounded-[48px] mr-20 '>
                    <div className='flex justify-center items-center w-40 md:w-80 h-32 md:h-52'>

                        <Telephone
                            className="w-10 md:w-20 h-10 md:h-20 "
                        />
                    </div>
                </div>

                <div className='flex flex-col justify-center w-80 '>
                    <h2 className='text-[#E14C4C] text-md md:text-xl font-semibold ' id='titleTeacherList'>2.&nbsp;Contactez</h2>
                    <p className=' font-medium  text-sm md:text-base text-[#868686] ' id='teacherLanguage'>Nos professeurs vous répondent en quelques heures.</p>
                </div>

            </div>


            <div className='flex w-full md:w-auto py-4 '>
                <div className='flex flex-col justify-center w-80 mr-20 '>
                    <h2 className='text-[#E14C4C] text-md md:text-xl font-semibold ' id='titleTeacherList'>3.&nbsp;Organisez</h2>
                    <p className=' font-medium  text-sm md:text-base text-[#868686] ' id='teacherLanguage'>Consultez librement les profils selon vos critères.</p>
                </div>

                <div className='bg-[#6ED698] w-40 md:w-80 h-32 md:h-52 rounded-[48px] '>
                <div className='flex justify-center items-center w-40 md:w-80 h-32 md:h-52'>

                    <Calendrier
                        className="w-10 sm:w-20 h-10 sm:h-20"
                    />
                </div>
                </div>
            </div>
        </div>

        

    </section>
  )
}

