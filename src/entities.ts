export interface User {
    id?: number;
    role?: string;
    mail: string;
    password: string;
    image?: string;
    name?:string;
    lastname?:string;
}
export interface Student {
    id?: number;
    name?: string;
    lastName?: string;
    idUser?: number;
}
export interface Teacher {
    language: string;
    image: string;
    id?: number;
    name?: string;
    lastName?: string;
    idUser?: number;
}
export interface Message {
    id?: number;
    content: string;
    author?: string;
    date?: string ;
    idTeacher?: number;
    idStudent?: number;
}

export interface Language {
    id?: number;
    name?: string;
}

