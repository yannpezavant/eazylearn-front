import axios from "axios";
import { Language, Teacher } from "./entities";


export const languageService = {
    async fetchAllLanguages(){
        const response = await axios.get<Language[]>("/api/language");
        return response.data;
    },
    async fetchLanguageByTeacher(id:number) {
        const response = await axios.get<Language[]>("/api/teacher/" + id + "/language");
        return response.data;
    }
} 

export async function fetchTeacherByLanguages(id:number) {
    const response = await axios.get<Teacher[]>("/api/language/" + id + "/teacher");
    return response.data;
}

// export async function fetchLanguageByTeacher(id:number) {
//     const response = await axios.get<Language[]>("/api/teacher/" + id + "/language");
//     return response.data;
// }

